package usecases

import "lab3/src/library/models"

type ILibraryUsecase interface {
	GetByCity(city string) ([]*models.Library, error)
	GetByUid(luid string) (*models.Library, error)
}
