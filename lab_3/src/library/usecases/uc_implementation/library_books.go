package uc_implementation

import "lab3/src/library/repositories"

type LibraryBooksUsecase struct {
	lbr repositories.ILibraryBooksRepository
}

func NewLibraryBooksUsecase(repo repositories.ILibraryBooksRepository) *LibraryBooksUsecase {
	return &LibraryBooksUsecase{ lbr: repo }
}

func (lbc *LibraryBooksUsecase) UpdateBooksAmount(luid string, buid string, amount int) error {
	return lbc.lbr.UpdateBooksAmount(luid, buid, amount)
}

func (lbc *LibraryBooksUsecase) GetBooksAmount(luid string, buid string) (int, error) {
	return lbc.lbr.GetBooksAmount(luid, buid)
}

func (lbc *LibraryBooksUsecase) ClaimBook(luid string, buid string) error {
	available, err := lbc.lbr.GetBooksAmount(luid, buid)
	if available > 0 {
		err = lbc.lbr.UpdateBooksAmount(luid, buid, available-1)
	}
	return err
}