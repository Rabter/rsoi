package repositories

import (
	"lab3/src/library/models"
)

type ILibraryRepository interface {
	GetByCity(city string) ([]*models.Library, error)
	GetByUid(luid string) (*models.Library, error)
}
