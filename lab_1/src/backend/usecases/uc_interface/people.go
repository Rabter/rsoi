package uc_interface

import "lab1/models"

type IPersonsUsecase interface {
	GetAll() ([]*models.Person, error)
	GetById(id int) (*models.Person, error)
	Create(person *models.InputPerson) (int, error)
	Update(id int, newInfo *models.InputPerson) (models.Person, int, error)
	Delete(id int) (int, error)
}
