package repo_interface

import "lab1/models"

type IPersonsRepository interface {
	Create(person *models.InputPerson) (int, error)
	GetById(id int) (*models.Person, error)
	GetAll() ([]*models.Person, error)
	Update(id int, person *models.InputPerson) (int, error)
	Delete(id int) (int, error)
}
