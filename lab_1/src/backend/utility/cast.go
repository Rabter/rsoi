package utility

import "encoding/binary"

func BytesToInt(data []byte) int {
	return int(binary.BigEndian.Uint32(data))
}

func BytesToString (data []byte) string {
	return string(data)
}