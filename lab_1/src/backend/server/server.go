package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"lab1/database/pgdb"
	"lab1/handlers"
	"lab1/repositories/repo_implementation"
	"lab1/usecases/uc_implementation"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func RunServer(address string, connectionString string) error {
	router := mux.NewRouter()
	apiRouter := router.PathPrefix("/api/v1").Subrouter()

	manager := pgdb.NewPGDBManager()
	err := manager.Connect(connectionString)
	if err != nil {
		fmt.Print("Failed to connect to database")
	} else {
		fmt.Println("Successfully connected to postgres database")
	}

	pr := repo_implementation.NewPersonsRepository(manager)
	pc := uc_implementation.NewPersonsUsecase(pr)
	ph := handlers.NewPerosnsHandlers(pc)

	apiRouter.HandleFunc("/persons", ph.PostPerson).Methods(http.MethodPost)
	apiRouter.HandleFunc("/persons", ph.GetPersons).Methods(http.MethodGet)
	apiRouter.HandleFunc("/persons/{id:[0-9]+}", ph.GetPersonById).Methods(http.MethodGet)
	apiRouter.HandleFunc("/persons/{id:[0-9]+}", ph.UpdatePerson).Methods(http.MethodPatch)
	apiRouter.HandleFunc("/persons/{id:[0-9]+}", ph.DeletePerson).Methods(http.MethodDelete)

	server := http.Server{
		Addr:    address,
		Handler: apiRouter,
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		manager.Disconnect()
		os.Exit(0)
	}()

	fmt.Printf("Server is running on %s\n", address)
	return server.ListenAndServe()
}